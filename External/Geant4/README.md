Geant 4
=======

This package builds Geant 4 together with some ATLAS specific modifications,
to be used by the offline software.

The Geant 4 sources are picked up from
https://gitlab.cern.ch/atlas-simulation-team/geant4.git, and are built with
the appropriate flags, against the right version of CLHEP and other externals.

Since we use a custom version of CLHEP at the moment, this package must be
built together with External/CLHEP.
