# Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
#
# Wrapper around Acts's auto-generated CMake configuration, which ensures that
# the runtime environment of Acts would be set up correctly.
#

# The LCG include(s).
include( LCGFunctions )

# Use the helper macro to do most of the work.
lcg_wrap_find_module( Acts
   NO_LIBRARY_DIRS
   IMPORTED_TARGETS ActsCore )
