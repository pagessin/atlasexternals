// This file's extension implies that it's C, but it's really -*- C++ -*-.
/*
 * Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration.
 */
/**
 * @file CheckerGccPlugins/src/feshim.h
 * @author scott snyder <snyder@bnl.gov>
 * @date Apr, 2024
 * @brief Shims for accessing front-end symbols.
 *
 * In the cmake builds, the -fplugin switch will be given for both
 * the compile and link steps.  Normally, when that switch is given
 * during linking, it will not have any effect.  But if link-time
 * optimization (LTO) is being done, then the compiler will run at link
 * time and the plugin gets loaded.  The problem then is that it's not
 * the full compiler that runs, but only the middle/backend.  If the
 * plugin references symbols from the frontend, then it will fail
 * to load during the link step, causing the link to fail.
 * (gcc will load the plugin with RTLD_NOW.)
 *
 * To resolve this, we launder references to FE symbols through
 * the shims defined here, which access the symbols via dlsym().
 * If the first symbol is missing then we return gracefully and
 * disable the checkers.
 */


#ifndef CHECKERGCCPLUGINS_FESHIM_H
#define CHECKERGCCPLUGINS_FESHIM_H


#include "tree-core.h"


struct cpp_reader;


namespace CheckerGccPlugins
{


//
// Initialize shims for accessing symbols from the front-end.
// Returns true on success, false if frontend symbols are not available.
//
bool init_shims();


// The shim functions.


typedef void (*pragma_handler_1arg)(struct cpp_reader *);
void c_register_pragma (const char *space, const char *name,
                        pragma_handler_1arg handler);

void cpp_define (const char* def);


tree strip_typedefs (tree t,
                     bool* remove_attributes = NULL,
                     unsigned int flags = 0);

tree lookup_base (tree t, tree base);

const char* type_as_string (tree t, int flags);

const char* decl_as_string (tree t, int flags);

tree skip_artificial_parms_for (const_tree fn, tree list);

tree
dfs_walk_all (tree binfo, tree (*pre_fn) (tree, void *),
              tree (*post_fn) (tree, void *), void *data);


int cp_type_quals (const_tree t);


tree look_for_overrides_here (tree type, tree fndecl);


tree get_current_namespace();


} // namespace CheckerGccPlugins


// Define this here so that the macros in cp-tree.h that use this function
// will pick up the shim instead.
#ifndef FESHIM_CXX
#define cp_type_quals CheckerGccPlugins::cp_type_quals
#endif


#endif // not CHECKERGCCPLUGINS_FESHIM_H
