# Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
#
# Sets:
#  FLAKE8_BUILTINS_PYTHON_PATH
#
# Can be steered by FLAKE8_BUILTINS_LCGROOT.
#

# The LCG include(s).
include( LCGFunctions )

# Find it.
lcg_python_external_module( NAME flake8_builtins
   MODULE_NAME flake8_builtins
   PYTHON_NAMES flake8_builtins.py flake8_builtins/__init__.py )

# Handle the standard find_package arguments.
include( FindPackageHandleStandardArgs )
find_package_handle_standard_args( flake8_builtins DEFAULT_MSG
   _FLAKE8_BUILTINS_PYTHON_PATH )

# Set up the RPM dependency.
lcg_need_rpm( flake8_builtins )
