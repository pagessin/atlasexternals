# Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
#
# Package building SoQt for ATLAS.
#

# The package's name:
atlas_subdir( Coin3D )

# Silence ExternalProject warnings with CMake >=3.24.
if( POLICY CMP0135 )
   cmake_policy( SET CMP0135 NEW )
endif()

# Directory for the temporary build results:
set( _buildDir
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/coin3dBuild" )

# Declare where to get Coin3D from.
set( ATLAS_COIN3D_SOURCE
   "URL;http://cern.ch/atlas-software-dist-eos/externals/Coin3D/coin-5297f6c_2023Apr18.zip;URL_MD5;4a118713dfbe09d39ad8118de0ad6fed"
   CACHE STRING "The source for Coin3D" )
mark_as_advanced( ATLAS_COIN3D_SOURCE )

# Decide whether / how to patch the Coin3D sources.
set( ATLAS_COIN3D_PATCH "" CACHE STRING "Patch command for Coin3D" )
set( ATLAS_COIN3D_FORCEDOWNLOAD_MESSAGE
   "Forcing the re-download of Coin3D (2023.08.15.)"
   CACHE STRING "Download message to update whenever patching changes" )
mark_as_advanced( ATLAS_COIN3D_PATCH ATLAS_COIN3D_FORCEDOWNLOAD_MESSAGE )

# Set extra CMake arguments for Coin3D.
set( _extraArgs )
if( NOT "${CMAKE_BUILD_TYPE}" STREQUAL "" )
   list( APPEND _extraArgs -DCMAKE_BUILD_TYPE:STRING=${CMAKE_BUILD_TYPE} )
endif()
if( NOT "${CMAKE_CXX_STANDARD}" STREQUAL "" )
   list( APPEND _extraArgs -DCMAKE_CXX_STANDARD:STRING=${CMAKE_CXX_STANDARD} )
endif()

if( ATLAS_BUILD_BOOST )
   list( APPEND _extraArgs
      -DBOOST_ROOT:PATH=${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM} )
else()
   find_package( Boost CONFIG )
   list( APPEND _extraArgs
      -DBOOST_ROOT:PATH=${Boost_DIR} )
endif()

# Set up the build of Coin3D.
ExternalProject_Add( Coin3D
   PREFIX "${CMAKE_BINARY_DIR}"
   INSTALL_DIR "${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}"
   ${ATLAS_COIN3D_SOURCE}
   ${ATLAS_COIN3D_PATCH}
   CMAKE_CACHE_ARGS
   -DSIMAGE_DIR:PATH=${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
   -DSIMAGE_RUNTIME_LINKING:BOOL=ON
   -DCMAKE_INSTALL_PREFIX:PATH=${_buildDir}
   -DCMAKE_INSTALL_LIBDIR:PATH=${CMAKE_INSTALL_LIBDIR}
   -DCOIN_BUILD_DOCUMENTATION:BOOL=false
   ${_extraArgs}
   LOG_CONFIGURE 1 )
ExternalProject_Add_Step( Coin3D forcedownload
   COMMAND ${CMAKE_COMMAND} -E echo "${ATLAS_COIN3D_FORCEDOWNLOAD_MESSAGE}"
   DEPENDERS download )
ExternalProject_Add_Step( Coin3D purgeBuild
   COMMAND ${CMAKE_COMMAND} -E remove_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E make_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E remove_directory "${_buildDir}"
   COMMENT "Removing previous build results for CLHEP"
   DEPENDEES download
   DEPENDERS patch )
ExternalProject_Add_Step( Coin3D forceconfigure
   COMMAND ${CMAKE_COMMAND} -E remove -f "<BINARY_DIR>/CMakeCache.txt"
   COMMENT "Forcing the (re-)configuration of CLHEP"
   DEPENDEES update
   DEPENDERS configure
   ALWAYS 1 )
ExternalProject_Add_Step( Coin3D buildinstall
   COMMAND ${CMAKE_COMMAND} -E copy_directory "${_buildDir}/" "<INSTALL_DIR>"
   COMMENT "Installing Coin3D into the build area"
   DEPENDEES install  )

# Set dependencies
if( ATLAS_BUILD_BOOST )
   add_dependencies( Coin3D Boost )
endif()
add_dependencies( Coin3D Simage )
add_dependencies( Package_Coin3D Coin3D )

# Install Coin3D:
install( DIRECTORY "${_buildDir}/"
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )
