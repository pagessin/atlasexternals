GeoModel Library
================

This package builds the GeoModel libraries for the offline software of ATLAS.

The library's sources are taken from:
https://gitlab.cern.ch/GeoModelDev/GeoModel
