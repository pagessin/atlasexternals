# Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
#
# Package building YAMPL for ATLAS.
#

# The package's name:
atlas_subdir( yampl )

# Silence ExternalProject warnings with CMake >=3.24.
if( POLICY CMP0135 )
   cmake_policy( SET CMP0135 OLD )
endif()

# Declare where to get yampl from.
set( ATLAS_YAMPL_SOURCE
   "URL;http://cern.ch/atlas-software-dist-eos/externals/yampl/yampl-v1.2.tar.bz2;URL_MD5;67c4321a4ef0bd8e5280462443752e70"
   CACHE STRING "The source for yampl" )
mark_as_advanced( ATLAS_YAMPL_SOURCE )

# Decide whether / how to patch the yampl sources.
set( ATLAS_YAMPL_PATCH "" CACHE STRING "Patch command for yampl" )
set( ATLAS_YAMPL_FORCEDOWNLOAD_MESSAGE
   "Forcing the re-download of yampl (2024.1.10.)"
   CACHE STRING "Download message to update whenever patching changes" )
mark_as_advanced( ATLAS_YAMPL_PATCH ATLAS_YAMPL_FORCEDOWNLOAD_MESSAGE )

# External(s) needed by yampl.
find_package( UUID )

# Directory for the temporary build results:
set( _buildDir
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/yamplBuild" )

# Set up the build of YAMPL.
ExternalProject_Add( yampl
   PREFIX "${CMAKE_BINARY_DIR}"
   INSTALL_DIR "${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}"
   ${ATLAS_YAMPL_SOURCE}
   ${ATLAS_YAMPL_PATCH}
   BUILD_IN_SOURCE 1
   CONFIGURE_COMMAND "<SOURCE_DIR>/configure" --prefix=${_buildDir}
   CXXFLAGS=-I${UUID_INCLUDE_DIR} LDFLAGS=${UUID_LIBRARIES}
   INSTALL_COMMAND make install
   COMMAND ${CMAKE_COMMAND} -E copy_directory "${_buildDir}/" "<INSTALL_DIR>" )
ExternalProject_Add_Step( yampl forcedownload
   COMMAND ${CMAKE_COMMAND} -E echo "${ATLAS_YAMPL_FORCEDOWNLOAD_MESSAGE}"
   DEPENDERS download )
add_dependencies( Package_yampl yampl )

# Install YAMPL:
install( DIRECTORY "${_buildDir}/"
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )

# Install its find-module:
install( FILES "cmake/Findyampl.cmake"
   DESTINATION "${CMAKE_INSTALL_CMAKEDIR}/modules" )
