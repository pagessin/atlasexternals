# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
#
# This script is used to make the LHAPDF scripts forget about the build
# directories, and rely on some environment variables set by the current
# project instead.
#

# The first file to patch:
set( _fileName
   "$ENV{DESTDIR}/${CMAKE_INSTALL_PREFIX}/@CMAKE_INSTALL_BINDIR@/lhapdf-config" )

# Check if the file exists:
if( EXISTS ${_fileName} )

   # Read in the generated configuration file:
   file( READ ${_fileName} _content LIMIT 20000000 )

   # Replace all occurences of the build directory with the installation path
   # in the configuration file:
   set( _outputContent )
   foreach( _element ${_content} )
      # Do the replacement:
      string( REPLACE "@_buildDir@" "\${@CMAKE_PROJECT_NAME@_DIR}"
         _newContent ${_element} )
      # It's done like this to make it clear to CMake that the semicolon
      # is to be kept in the string. And not to be used as a list separator.
      if( _outputContent )
         set( _outputContent "${_outputContent}\;${_newContent}" )
      else()
         set( _outputContent "${_newContent}" )
      endif()
   endforeach()

   # Overwrite the original file:
   file( WRITE ${_fileName} ${_outputContent} )

else()
   message( WARNING "Didn't find file: ${_fileName}" )
endif()

# The second file to patch:
set( _fileName
   "$ENV{DESTDIR}/${CMAKE_INSTALL_PREFIX}/@CMAKE_INSTALL_BINDIR@/lhapdf" )

# Check if the file exists:
if( EXISTS ${_fileName} )

   # Read in the generated configuration file:
   file( READ ${_fileName} _content LIMIT 20000000 )

   # Replace all occurences of the build directory with the installation path
   # in the configuration file:
   set( _outputContent )
   foreach( _element ${_content} )
      # Do the replacement:
      string( REGEX REPLACE "configured_datadir = '[^\n]*\n"
         "configured_datadir = os.environ['@CMAKE_PROJECT_NAME@_DIR'] + '/share/LHAPDF'\n"
         _newContent ${_element} )
      # It's done like this to make it clear to CMake that the semicolon
      # is to be kept in the string. And not to be used as a list separator.
      if( _outputContent )
         set( _outputContent "${_outputContent}\;${_newContent}" )
      else()
         set( _outputContent "${_newContent}" )
      endif()
   endforeach()

   # Overwrite the original file:
   file( WRITE ${_fileName} ${_outputContent} )

else()
   message( WARNING "Didn't find file: ${_fileName}" )
endif()

# Clean up:
unset( _fileName )
unset( _content )
unset( _outputContent )
unset( _newContent )
