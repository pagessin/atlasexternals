# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#
# Locate the lwtnn external package.
#
# Defines:
#  LWTNN_FOUND
#  LWTNN_INCLUDE_DIR
#  LWTNN_INCLUDE_DIRS
#  LWTNN_LIBRARIES
#  LWTNN_LIBRARY_DIRS
#
# The user can set LWTNN_ATROOT to guide the script.
#

# Include the helper code:
include( AtlasInternals )

# Declare the module:
atlas_external_module( NAME lwtnn
   INCLUDE_SUFFIXES include INCLUDE_NAMES lwtnn
   LIBRARY_SUFFIXES lib
   COMPULSORY_COMPONENTS lwtnn )

# Handle the standard find_package arguments:
include( FindPackageHandleStandardArgs )
find_package_handle_standard_args( lwtnn DEFAULT_MSG LWTNN_INCLUDE_DIRS
   LWTNN_LIBRARIES )
mark_as_advanced( LWTNN_FOUND LWTNN_INCLUDE_DIR LWTNN_INCLUDE_DIRS
   LWTNN_LIBRARIES LWTNN_LIBRARY_DIRS )

if( LWTNN_FOUND )
   find_package( Eigen )
   if( EIGEN_INCLUDE_DIRS )
      list( APPEND LWTNN_INCLUDE_DIRS ${EIGEN_INCLUDE_DIRS} )
   endif()
endif()
