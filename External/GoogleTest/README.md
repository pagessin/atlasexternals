Google Test
===========

This package builds the GoogleTest and GoogleMock libraries. To be used by
the unit tests of the offline software.

The source code is downloaded from https://github.com/google/googletest.git
directly.
