# Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
#
# Package building SoQt for ATLAS.
#

# The package's name.
atlas_subdir( SoQt )

# Silence ExternalProject warnings with CMake >=3.24.
if( POLICY CMP0135 )
   cmake_policy( SET CMP0135 NEW )
endif()

# External dependencies.
find_package( Qt5 COMPONENTS OpenGL )
find_package( OpenGL )

# Declare where to get SoQt from.
set( ATLAS_SOQT_SOURCE
   "URL;http://cern.ch/atlas-software-dist-eos/externals/SoQt/soqt_ea5cd76.zip;URL_MD5;8e0f5dd1805587b00bc160be9f8aff7e"
   CACHE STRING "The source for SoQt" )
mark_as_advanced( ATLAS_SOQT_SOURCE )

# Decide whether / how to patch the SoQt sources.
set( ATLAS_SOQT_PATCH "" CACHE STRING "Patch command for SoQt" )
set( ATLAS_SOQT_FORCEDOWNLOAD_MESSAGE
   "Forcing the re-download of SoQt (2022.10.11.)"
   CACHE STRING "Download message to update whenever patching changes" )
mark_as_advanced( ATLAS_SOQT_PATCH ATLAS_SOQT_FORCEDOWNLOAD_MESSAGE )

# Directory for the temporary build results:
set( _buildDir
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/SoQtBuild" )

# Extra configuration parameters.
set( _extraConf )
if( NOT "${CMAKE_BUILD_TYPE}" STREQUAL "" )
   list( APPEND _extraConf -DCMAKE_BUILD_TYPE:STRING=${CMAKE_BUILD_TYPE} )
endif()
if( NOT "${CMAKE_CXX_STANDARD}" STREQUAL "" )
   list( APPEND _extraConf -DCMAKE_CXX_STANDARD:STRING=${CMAKE_CXX_STANDARD} )
endif()

# List of paths given to CMAKE_PREFIX_PATH.
set( _prefixPaths "${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}"
   $ENV{CMAKE_PREFIX_PATH} ${QT5_LCGROOT} )

# Build SoQt.
ExternalProject_Add( SoQt
   PREFIX "${CMAKE_BINARY_DIR}"
   INSTALL_DIR "${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}"
   ${ATLAS_SOQT_SOURCE}
   ${ATLAS_SOQT_PATCH}
   CMAKE_CACHE_ARGS
   -DCMAKE_PREFIX_PATH:PATH=${_prefixPaths}
   -DCMAKE_INSTALL_PREFIX:PATH=${_buildDir}
   -DCMAKE_INSTALL_BINDIR:PATH=${CMAKE_INSTALL_BINDIR}
   -DCMAKE_INSTALL_LIBDIR:PATH=${CMAKE_INSTALL_LIBDIR}
   -DSOQT_BUILD_DOCUMENTATION:BOOL=FALSE
   ${_extraConf}
   LOG_CONFIGURE 1 )
ExternalProject_Add_Step( SoQt forcedownload
   COMMAND ${CMAKE_COMMAND} -E echo "${ATLAS_SOQT_FORCEDOWNLOAD_MESSAGE}"
   DEPENDERS download )
ExternalProject_Add_Step( SoQt purgeBuild
   COMMAND ${CMAKE_COMMAND} -E remove_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E make_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E remove_directory "${_buildDir}"
   COMMENT "Removing previous build results for SoQt"
   DEPENDEES download
   DEPENDERS patch )
ExternalProject_Add_Step( SoQt forceconfigure
   COMMAND ${CMAKE_COMMAND} -E remove -f "<BINARY_DIR>/CMakeCache.txt"
   COMMENT "Forcing the configuration of SoQt"
   DEPENDEES update
   DEPENDERS configure
   ALWAYS 1 )
ExternalProject_Add_Step( SoQt buildinstall
   COMMAND ${CMAKE_COMMAND} -E copy_directory "${_buildDir}/" "<INSTALL_DIR>"
   COMMENT "Installing SoQt into the build area" 
   DEPENDEES install )
add_dependencies( SoQt Coin3D )
add_dependencies( Package_SoQt SoQt )

# Install SoQt.
install( DIRECTORY "${_buildDir}/"
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )
