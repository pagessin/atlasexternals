// Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
// thread16_test: testing check_virtual_overrides

#pragma ATLAS check_thread_safety

struct B
{
  virtual void f1 [[ATLAS::not_reentrant]] ();
  virtual void f2();
  virtual void f3();
};


struct D : public B
{
  virtual void f1();
  virtual void f2 [[ATLAS::not_reentrant]] ();
  virtual void f3 [[ATLAS::not_const_thread_safe]] ();
};

struct X { virtual ~X(); };

struct D2 : public B, public X
{
  virtual void f2 [[ATLAS::not_reentrant]] ();
};

namespace Gaudi {
class Algorithm {
  virtual int start();
  virtual int stop();
  virtual int foo();
  virtual int initialize();
  virtual int finalize();
  virtual int execute();
  virtual int execute(int) const;
};
}
class Service {
  virtual int foo();
  virtual int initialize();
  virtual int finalize();
};
class AlgTool {
  virtual int foo();
  virtual int initialize();
  virtual int finalize();
};
class AthService: public Service {};
class AthAlgorithm: public Gaudi::Algorithm {};
class AthAlgTool: public AlgTool {};


struct TestAlg : public AthAlgorithm
{
  virtual int start [[ATLAS::not_thread_safe]]();
  virtual int stop [[ATLAS::not_thread_safe]]();
  virtual int foo [[ATLAS::not_thread_safe]]();
  virtual int initialize [[ATLAS::not_thread_safe]]();
  virtual int finalize [[ATLAS::not_thread_safe]]();
  virtual int execute [[ATLAS::not_thread_safe]] ();  // no warning
  virtual int execute [[ATLAS::not_thread_safe]] (int) const; // warning
};


struct TestService : public AthService
{
  virtual int foo [[ATLAS::not_thread_safe]]();
  virtual int initialize [[ATLAS::not_thread_safe]]();
  virtual int finalize [[ATLAS::not_thread_safe]]();
};


struct TestAlgTool : public AthAlgTool
{
  virtual int foo [[ATLAS::not_thread_safe]]();
  virtual int initialize [[ATLAS::not_thread_safe]]();
  virtual int finalize [[ATLAS::not_thread_safe]]();
};
