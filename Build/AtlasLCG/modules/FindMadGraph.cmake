# Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
#
# Module for finding MadGraph.
#

# The LCG include(s):
include( LCGFunctions )

# Declare the external module:
lcg_external_module( NAME MadGraph
   BINARY_NAMES "mg5_aMC"
   BINARY_SUFFIXES "bin" "bin64"
   SEARCH_PATHS ${MADGRAPH5AMC_LCGROOT} )

# Handle the standard find_package arguments:
include( FindPackageHandleStandardArgs )
find_package_handle_standard_args( MadGraph DEFAULT_MSG
   MADGRAPH_mg5_aMC_EXECUTABLE MADGRAPH_BINARY_PATH )
mark_as_advanced( MADGRAPH_FOUND )

# Set up the RPM dependency:
lcg_need_rpm( madgraph5amc FOUND_NAME MADGRAPH VERSION_NAME MADGRAPH5AMC )

# Set the MADPATH environment variable:
if( MADGRAPH_FOUND )
   set( MADGRAPH_ENVIRONMENT
      FORCESET MADPATH ${MADGRAPH5AMC_LCGROOT} )
endif()
